Dado('que faço um GET na API') do
    @url = 'https://api-de-tarefas.herokuapp.com/contacts'
    @headers = {
        'Conten-type': 'application/json',
        'Accept': 'application/vnd.tasksmanager.v2'
    }

    @response = HTTParty.get(@url, headers: @headers)
    
end

Então('recebo um resultado') do
    @code_return = 200
    @teste_c = @response.body
    raise "Erro: o codido retornado esta errado!" if @code_return != @response.code
    raise "Erro: A api esta retornando vario" if @response.body == @teste_c.empty?
    end
